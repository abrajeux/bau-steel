import {BrowserRouter, Routes, Route} from "react-router-dom";
import NavBar from "./components/navigation/navbar";
import Home from "./routes/home";
import {ParallaxProvider} from "react-scroll-parallax";
import Footer from "./components/navigation/footer";
import Contact from "./routes/contact";
import Projects from "./routes/projects";

function App() {
  return (
      <ParallaxProvider>
          <BrowserRouter basename={process.env.PUBLIC_URL ?? "/"}>
            <div className="bg-black h-full w-screen text-white px-32 pb-24 space-y-16">
                <NavBar />
                <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="work" element={<Projects/>} />
                    <Route path="contact" element={<Contact/>} />
                    <Route path="about" element={<h1>About</h1>} />
                </Routes>
                <Footer/>
            </div>
          </BrowserRouter>
      </ParallaxProvider>
  );
}

export default App;
