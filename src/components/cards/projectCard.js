import React from 'react';
import PropTypes from "prop-types";
import ArrowLink from "../navigation/arrowLink";

const ProjectCard = ({title,description, image, tag, id, width, height}) => {

    return (
        <div
            className={`col-span-${width ?? '2'} row-span-${height ?? '1'} w-full h-full`}
            key={id}
        >
            <div className={`flex flex-col justify-between border border-white w-full h-full`}>
                <div className={"relative cursor-pointer bg-orange overflow-hidden h-full"}>
                    {tag && <div
                        className={"pointer-events-none absolute top-4 right-4 z-10 bg-white py-1 px-2 hover:bg-orange-light border-2 border-white hover:border-orange transition-all "}
                    >
                        <p className={"text-orange font-semibold uppercase text-sm"}>{tag}</p>
                    </div>}
                    <img
                        src={image}
                        className={"hover:opacity-70 opacity-100 transition-all  w-full h-full flex-none object-cover"}
                        alt={"project_img"}
                    />
                </div>
                <div className={"space-y-4 border-t border-white p-4"}>
                    <p className={"text-2xl font-semibold"}>{title}</p>
                    <p className={"text-md"}>{description}</p>
                    <div className={"w-full flex justify-end"}>
                        <ArrowLink path={"/work/" + id} text={"En savoir plus"} direction={"right"}/>
                    </div>
                </div>
            </div>
        </div>
    );
};

ProjectCard.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    tag: PropTypes.string,
    id: PropTypes.string.isRequired
};

ProjectCard.defaultProps = {
    tag: null
}

export default ProjectCard;