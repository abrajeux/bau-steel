import React from 'react';
import {Link} from 'react-router-dom';
import {ArrowLeft, ArrowRight} from "react-feather";
import PropTypes from "prop-types";

const ArrowLink = ({path, text, direction}) => {
    return (
            <Link
                to={path}
                className={`w-content group text-white hover:text-orange flex items-center justify-end ${direction === "left" ? "flex-row-reverse" : "flex-row space-x-3 "}`}
            >
                <p className={"text-sm uppercase"}>{text}</p>
                <div className={"flex items-center"}>
                    {direction === "left" && <ArrowLeft className={"h-6 w-6 flex-none"}/>}
                    <div className={`w-0 group-hover:w-6 bg-orange h-0.5 flex-none rounded transition-all ${direction === "right" ? "-mr-2" :"-ml-2 mr-2"}`}/>
                    {direction === "right" && <ArrowRight className={"h-6 w-6 flex-none"}/>}
                </div>
            </Link>
    );
};

ArrowLink.propTypes = {
    path: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    direction: PropTypes.string.isRequired
};

ArrowLink.defaultProps = {
    direction: "right"
}

export default ArrowLink;