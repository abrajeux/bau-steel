import React from 'react';
import {ArrowUpRight} from "react-feather";
import {Link} from "react-router-dom";

const Footer = () => {
    return (
        <div className={"uppercase space-y-4 w-full"}>
            <Link
                className={"flex items-end space-x-2 group hover:text-orange transition-all"}
                to={"/contact"}
            >
                <p className={"text-lg font-semibold"}>Contactez-moi</p>
                <ArrowUpRight className={"h-6 w-6 group-hover:w-8 group-hover:h-8 transition-all group-hover:-mt-2"}/>
            </Link>
            <div className={"border-t border-white w-full"}/>
            <div className={"flex items-center justify-between"}>
                <p>Tous droits réservés Bausteel @2022</p>
                <div className={"flex items-center space-x-4"}>
                    <a href={"https://www.facebook.com/BauSteel75"}>Facebook</a>
                    <a href={"https://www.instagram.com/bau.steel/"}>Instagram</a>
                </div>
            </div>
        </div>
    );
};

export default Footer;