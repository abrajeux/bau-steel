import React from 'react';
import {Link, useLocation} from 'react-router-dom';
import NavLink from "./navlink";

const NavBar = () => {

    const location = useLocation();
    return (
        <div>
            <nav className={"flex justify-between items-center py-12"}>
                <Link to={"/"}>
                    <img src="img/logos/logo.png" alt="logo" className={"w-content"}/>
                </Link>
                <div className={"hidden lg:flex items-center justify-between w-1/2 max-w-lg space-x-4 uppercase font-bold"}>
                    <NavLink to={"/work"} selected={location.pathname === "/work"}>
                        <p className={"text-lg"}>Réalisations</p>
                    </NavLink>
                    <NavLink to={"/contact"} selected={location.pathname === "/contact"}>
                        <p className={"text-lg"}>Contact</p>
                    </NavLink>
                    <NavLink to={"/about"} selected={location.pathname === "/about"}>
                        <p className={"text-lg"}>A propos</p>
                    </NavLink>
                </div>
            </nav>
        </div>
    );
};

NavBar.propTypes = {
    
};

export default NavBar;