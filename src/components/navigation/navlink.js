import React from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

const NavLink = ({to,selected, children, onClick}) => {
    return (
        <div className={"group flex-none "+ (selected ? "text-orange " : "text-white")} onClick={onClick}>
            <Link to={to}>
                {children}
                <div
                    className={`group-hover:w-1/2 ${selected ? "w-1/2" : "w-0"} bg-orange h-0.5 transition-all rounded-sm`}
                />
            </Link>
        </div>
    );
};

NavLink.propTypes = {
    to: PropTypes.string.isRequired,
    selected: PropTypes.bool,
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func,
};

NavLink.defaultProps = {
    selected: false,
    onClick: () => {}
}

export default NavLink;