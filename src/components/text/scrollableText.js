import React from 'react';
import PropTypes from 'prop-types';
import {useParallax} from "react-scroll-parallax";

const ScrollableText = ({scroll, children}) => {

    const parallax = useParallax(scroll);

    return (
        <div
            ref={parallax.ref}
            className={"relative font-prompt text-black stroke-orange uppercase text-8xl whitespace-nowrap"}
            style={{WebkitTextStroke: "1px #FF7121"}}
        >
            {children}
        </div>
    );
};

ScrollableText.propTypes = {
    children: PropTypes.node.isRequired
};

ScrollableText.defaultProps = {

}

export default ScrollableText;