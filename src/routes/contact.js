import React from 'react';

const Contact = () => {
    return (
        <div className={"flex justify-between space-x-32"}>
            <div>
                <p>Pour tout vos travaux de métallerie ou de ferronnerie en Ile-De-France n'hésitez pas à me contacter !</p>
            </div>
            <div className={"w-2/5 space-y-8 flex-none"}>
                <div>
                    <p className={"font-semibold text-5xl pb-4 mb-4 border-b border-white w-full"}>MAIL</p>
                    <a href={"mailto:Bau.Steel@outlook.fr"} className={"font-semibold text-xl"}>Bau.Steel@outlook.fr</a>
                </div>
                <div>
                    <p className={"font-semibold text-5xl pb-4 mb-4 border-b border-white w-full"}>TÉLÉPHONE</p>
                    <p className={"font-semibold text-xl"}>06 67 95 21 46</p>
                </div>
                <div>
                    <p className={"font-semibold text-5xl pb-4 mb-4 border-b border-white w-full"}>LOCALISATION</p>
                    <p className={"font-semibold text-xl"}>Paris</p>
                </div>
                <div className={"flex flex-col"}>
                    <p className={"font-semibold text-5xl pb-4 mb-4 border-b border-white w-full"}>RÉSEAUX SOCIAUX</p>
                    <a href={"https://www.instagram.com/bau.steel/"} className={"font-semibold text-xl"}>Instagram</a>
                    <a href={"https://www.facebook.com/BauSteel75"} className={"font-semibold text-xl"}>Facebook</a>
                </div>
            </div>
        </div>
    );
};

export default Contact;