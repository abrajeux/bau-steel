import React from 'react';
import ScrollableText from "../components/text/scrollableText";
import ProjectCard from "../components/cards/projectCard";
import ArrowLink from "../components/navigation/arrowLink";

const Home = () => {

    const projects = [
        {
            id: "rampes",
            title: "Rampes 1",
            description: "Rampes/Verrières/design produit/met,co/objets/architecture/garde crops",
            image: "https://www.keuka-studios.com/wp-content/uploads/2019/09/L-shaped-stairs-with-Glass-Railing.jpg",
            tag: "Rampes",
            width: 2,
            height: 2
        },
        {
            id: "rampes-2",
            title: "Rampes 2",
            description: "Rampes/Verrières/design produit/met,co/objets/architecture/garde crops",
            image: "https://www.keuka-studios.com/wp-content/uploads/2019/09/L-shaped-stairs-with-Glass-Railing.jpg",
            tag: "Verrières",
            width: 1
        },
        {
            id: "rampes-3",
            title: "Rampes 3",
            description: "Rampes/Verrières/design produit/met,co/objets/architecture/garde crops",
            image: "https://www.keuka-studios.com/wp-content/uploads/2019/09/L-shaped-stairs-with-Glass-Railing.jpg",
            tag: "Verrières",
            width: 1
        },
    ];

    return (
        <div className={"w-full space-y-32"}>
            <div className={"flex justify-between relative z-10"}>
                <img alt={"hero_bg"} src={"img/hero_bg.png"} className={"absolute left-0 z-0 w-3/4"}/>
                <div className={"w-1/2 z-10"}>
                    <div className={"py-36 font-semibold text-8xl w-96 mx-auto"}>
                        <p className={"text-left"}>BAU</p>
                        <p className={"text-right"}>STEEL</p>
                    </div>
                    <p className={"border-t border-b border-white py-6"}>
                        Fondé par Thibault Baudrillart, l'atelier Bau Steel
                        conçoit et réalise <span>vos ouvrages
                        de métallerie et de ferronnerie d'art </span>
                        pour les professionels et les particuliers.
                    </p>
                </div>
                <div className={"w-1/2 relative z-20"}>
                    <img alt={"hero_1"} src={"img/hero_0.png"} className={"absolute top-24 left-24 z-20"}/>
                    <img alt={"hero_2"} src={"img/hero_1.png"} className={"absolute top-0 left-80 z-10"}/>
                </div>
            </div>
            <div>
                <ScrollableText scroll={{translateX:[-60,10]}}>
                    <p>Rampes/Verrières/design produit/met,co/objets/architecture/garde crops</p>
                </ScrollableText>
                <ScrollableText scroll={{translateX:[-10,-80]}}>
                    <p>Rampes/Verrières/design produit/met,co/objets/architecture/garde crops</p>
                </ScrollableText>
            </div>
            <div className={"space-y-4"}>
                <p className={"uppercase font-semibold text-5xl"}>Dernières <br/> réalisations</p>
                <div className={"grid grid-cols-3 gap-4 grid-flow-row-dense"}>
                    {projects.map((project) => (
                        <ProjectCard key={project.id} {...project}/>
                    ))}
                </div>
                <div className={"w-full flex justify-end"}>
                    <ArrowLink path={"/work"} text={"Voir tous les projets"}/>
                </div>
            </div>
            <div>
                <p className={"uppercase font-semibold text-5xl mb-12 w-96"}>C'est aussi un savoir faire...</p>
                <div>

                </div>
            </div>
            <ScrollableText scroll={{translateX:[-100,-50]}}>
                <p>à propos de bausteel/à propos de bausteel/à propos de bausteel/à propos de bausteel</p>
            </ScrollableText>
            <div className={"space-y-6"}>
                <p className={"uppercase font-semibold text-5xl mb-12 w-96"}>à propos de bausteel</p>
                <div className={"flex items-center justify-between space-x-32"}>
                    <div className={"border-b border-t border-white space-y-4 py-4 w-1/2"}>
                        <p>Fondée par Thibault Baudrillart</p>
                        <div className={"border-b border-white w-full"}></div>
                        <p>Paris</p>
                    </div>
                    <p className={"w-1/2 font-semibold text-2xl"}>
                        Suite à l’obtention d’un BMA ferronerie d’art, Thibault Baudrillart fonde <span>Bau Steel. </span>
                        Ayant grandi dans une famille d’artisans et fort d’une formation artistique riche, il se fera un
                        point d’orgue <span> à réaliser vos projets le plus fidèlement possible à vos envies.</span> Il a pour mission
                        de fournir des produits de qualité sur mesure aux finitions irréprochables.
                    </p>
                </div>
                <div className={"flex justify-center space-x-4 gap-5"}>
                    <img alt={"hero_3"} src={"img/hero_2.png"} className={"flex-none h-content"}/>
                    <img alt={"hero_4"} src={"img/hero_3.png"} className={"flex-none"}/>
                </div>
            </div>
        </div>
    );
};

export default Home;