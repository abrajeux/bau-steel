import React, {useEffect, useState} from 'react';
import NavLink from "../components/navigation/navlink";
import ProjectCard from "../components/cards/projectCard";

const Projects = () => {

    const [tags, setTags] = useState([]);
    const [projects, setProjects] = useState([]);
    const [selectedTag, setSelectedTag] = useState(null);

    useEffect(() => {
        setSelectedTag(null);
        setProjects([{
            id: "rampes",
            title: "Rampes 1",
            description: "Rampes/Verrières/design produit/met,co/objets/architecture/garde crops",
            image: "https://www.keuka-studios.com/wp-content/uploads/2019/09/L-shaped-stairs-with-Glass-Railing.jpg",
            tag: "Rampes",
            width: 2,
            height: 2
        },
            {
                id: "rampes-2",
                title: "Rampes 2",
                description: "Rampes/Verrières/design produit/met,co/objets/architecture/garde crops",
                image: "https://www.keuka-studios.com/wp-content/uploads/2019/09/L-shaped-stairs-with-Glass-Railing.jpg",
                tag: "Verrières",
                width: 1
            },
            {
                id: "rampes-3",
                title: "Rampes 3",
                description: "Rampes/Verrières/design produit/met,co/objets/architecture/garde crops",
                image: "https://www.keuka-studios.com/wp-content/uploads/2019/09/L-shaped-stairs-with-Glass-Railing.jpg",
                tag: "Verrières",
                width: 1
            },]);
        setTags(["Rampes","Verrières","design produit","Objets","architecture","garde crops"]);
    },[])

    return (
        <div className={"w-full"}>
            <p className={"uppercase font-semibold text-5xl mb-12 w-96"}>Réalisations</p>
            <div className={"flex space-x-3 items-center w-full mb-4"}>
                <NavLink
                    to={''}
                    onClick={() => setSelectedTag(null)}
                    selected={selectedTag === null}
                >
                    TOUT
                </NavLink>
                {tags.map(tag => (
                    <NavLink
                        to={''}
                        key={tag}
                        onClick={() => setSelectedTag(tag)}
                        selected={selectedTag === tag}
                    >{tag.toUpperCase()}</NavLink>)
                )}
                <div className={"border-b-2 border-white w-full h-1"}/>
            </div>
            <div className={"grid grid-cols-3 gap-4 grid-flow-row-dense"}>
                {projects.filter(p => (selectedTag === null || p.tag === selectedTag)).map(project => (<ProjectCard
                    image={project.image}
                    description={project.description}
                    id={project.id}
                    title={project.title}
                    tag={selectedTag === null ? project.tag : null}
                    width={project.width}
                    height={project.height}
                    />))}
            </div>
        </div>
    );
};

Projects.propTypes = {
    
};

export default Projects;