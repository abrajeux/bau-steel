module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        black: "#0D0C0C",
        orange: {
          DEFAULT : "#FF7121",
          light: "#FFEEE3",
        },
        white: {
          DEFAULT: "#FFFFFF",
          light: "rgba(248,248,248,0.8)",
        }
      },
      fontFamily: {
        prompt: ["Prompt", "sans-serif"],
      },
      width: {
        content: 'fit-content',
      },
      height: {
        content: 'fit-content',
      }
    },
  },
  plugins: [],
}